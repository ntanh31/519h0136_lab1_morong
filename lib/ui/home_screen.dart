import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:lab1_toeic/module/toeic_data.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}



class _HomeScreenState extends State<HomeScreen> {

  Future fetchToeic() async {
    final response = await http.get(Uri.parse('https://thachln.github.io/toeic-data/ets/2016/1/p1/data.json'));

    var jsonRaw = response.body;
    var jsonObject = json.decode(jsonRaw);

    List<ToeicData> datas = [];

    for(var i in jsonObject){
      ToeicData data = ToeicData.fromJson(i);
      datas.add(data);
    }
    return datas;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(title: Text("Toeic"),),
          body: Card(
            child: FutureBuilder(
              future: fetchToeic(),
              builder: (BuildContext context, AsyncSnapshot snapshot){
                if(snapshot.data == null){
                  return Center(
                    child: Text("Đang Tải"),
                  );
                }
                else
                  return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, index){
                        return Container(
                          color: index % 2 == 0 ? Colors.cyan : Colors.deepOrange,
                          child: Text(
                            snapshot.data[index].no.toString() + "  " + snapshot.data[index].image.toString() + "  " + snapshot.data[index].audio.toString() + "\n\n", style: TextStyle(fontSize: 20),
                          ),
                        );
                      }
                  );
              },
            ),
          )
      ),
    );
  }
}
