import 'dart:convert';
class ToeicData{
  final int no;
  final String audio;
  final String image;

  const ToeicData({
    required this.no,
    required this.image,
    required this.audio,
  });

  factory ToeicData.fromJson(json) {
    return ToeicData(
      no: json['no'],
      image: json['image'],
      audio: json['audio'],
    );
  }
}